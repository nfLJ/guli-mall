package com.nflj.gulimall.member.feign;

import com.nflj.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/5 10:32
 */

@FeignClient("gulimall-coupon")
public interface CouponFeignService {


    @GetMapping("/coupon/coupon/member/list")
    public R memberCoupons();
}
