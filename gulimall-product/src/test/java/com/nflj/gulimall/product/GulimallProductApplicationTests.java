package com.nflj.gulimall.product;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.nflj.gulimall.product.entity.BrandEntity;
import com.nflj.gulimall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * 使用 OSS 上传文件步骤
 * 1、引入对象存储 oss-starter
 * 2、配置 key、endpoint相关信息即可
 * 3、使用 OSSClient 进行相关操作
 */
@SpringBootTest
class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;



    @Test
    void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setName("小米");
        brandService.save(brandEntity);
        List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1));
        list.forEach((item)->{
            System.out.println(item);
        });
    }

}
