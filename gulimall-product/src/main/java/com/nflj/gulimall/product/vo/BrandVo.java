package com.nflj.gulimall.product.vo;

import lombok.Data;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 17:27
 */
@Data
public class BrandVo {
    /**
     * "brandId": 0,
     * "brandName": "string",
     */
    private Long brandId;
    private String  brandName;
}
