package com.nflj.gulimall.product.dao;

import com.nflj.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 21:22:04
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
