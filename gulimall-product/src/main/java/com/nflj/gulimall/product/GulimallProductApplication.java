package com.nflj.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 逻辑删除
 * 1）配置全局的逻辑删除规则
 * 2）配置逻辑删除的组件Bean
 * 3）给Bean加上逻辑删除注解 @TableLogic
 *
 * JSR303
 * 1）给 Bean 添加校验注解，并定义自己的 message
 * 2）开启校验功能 @Valid
 *  效果：校验错误后会有默认的响应
 * 3）给校验的 Bean 后紧跟一个 BindingResult,就可以获取到校验的结果
 * 4)分组校验(多场景的复杂校验）
 *      1、@NotBlank(message = "品牌名必须提交",groups = {AddGroup.class, UpdateGroup.class})
 *      给校验注解标注什么情况需要进行校验
 *      2、@Validated({AddGroup.class})
 *      3、默认没有指定分组的校验注解 @NotBlank，在分组校验情况@Validated({AddGroup.class})下不生效
 * 统一的异常处理@ControllerAdvice
 *
 * 自定义校验
 * 1）编写一个自定义校验注解
 * 2）编写一个自定义校验器 ConstraintValidator
 * 3）关联自定义校验器和和自定义校验注解
 * @Documented
 * @Constraint(validatedBy = {ListValueConstraintValidator.class【可以指定多个不同的校验器】})
 * @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
 * @Retention(RetentionPolicy.RUNTIME)
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.nflj.gulimall.product.feign")
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
