package com.nflj.gulimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 17:45
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;

}
