package com.nflj.gulimall.product.vo;

import lombok.Data;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/7 16:45
 */
@Data
public class AttrGroupRelationVo {
    //"attrId":1,"attrGroupId":2
    private Long attrId;
    private Long attrGroupId;
}
