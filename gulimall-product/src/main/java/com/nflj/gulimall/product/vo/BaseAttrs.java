package com.nflj.gulimall.product.vo;

import lombok.Data;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 17:44
 */
@Data
public class BaseAttrs {

    private Long attrId;
    private String attrValues;
    private int showDesc;
}
