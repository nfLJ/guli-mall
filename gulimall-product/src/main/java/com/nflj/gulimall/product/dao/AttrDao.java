package com.nflj.gulimall.product.dao;

import com.nflj.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 21:22:04
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
