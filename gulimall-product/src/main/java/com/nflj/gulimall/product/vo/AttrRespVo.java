package com.nflj.gulimall.product.vo;

import lombok.Data;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/7 16:44
 */
@Data
public class AttrRespVo extends AttrVo{
    /**
     * 			"catelogName": "手机/数码/手机", //所属分类名字
     * 			"groupName": "主体", //所属分组名字
     */
    private String catelogName;
    private String groupName;

    private Long[] catelogPath;
}
