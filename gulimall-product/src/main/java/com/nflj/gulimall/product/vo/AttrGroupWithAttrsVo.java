package com.nflj.gulimall.product.vo;

import com.nflj.gulimall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/7 16:59
 */
@Data
public class AttrGroupWithAttrsVo {

    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    private List<AttrEntity> attrs;
}
