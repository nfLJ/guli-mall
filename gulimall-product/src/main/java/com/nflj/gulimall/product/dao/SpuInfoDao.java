package com.nflj.gulimall.product.dao;

import com.nflj.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 21:22:04
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
	
}
