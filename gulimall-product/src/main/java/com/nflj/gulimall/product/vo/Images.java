package com.nflj.gulimall.product.vo;

import lombok.Data;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 17:45
 */
@Data
public class Images {
    private String imgUrl;
    private int defaultImg;
}
