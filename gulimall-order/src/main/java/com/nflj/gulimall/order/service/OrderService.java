package com.nflj.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nflj.common.utils.PageUtils;
import com.nflj.gulimall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:27:45
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

