package com.nflj.gulimall.order.dao;

import com.nflj.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:27:45
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
