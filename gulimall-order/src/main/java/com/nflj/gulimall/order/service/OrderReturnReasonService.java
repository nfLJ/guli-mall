package com.nflj.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nflj.common.utils.PageUtils;
import com.nflj.gulimall.order.entity.OrderReturnReasonEntity;

import java.util.Map;

/**
 * 退货原因
 *
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:27:45
 */
public interface OrderReturnReasonService extends IService<OrderReturnReasonEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

