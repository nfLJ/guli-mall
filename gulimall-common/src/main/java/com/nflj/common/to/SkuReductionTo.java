package com.nflj.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 18:02
 */
@Data
public class SkuReductionTo {

    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
