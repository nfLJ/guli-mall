package com.nflj.gulimall.coupon;

import com.nflj.gulimall.coupon.entity.CategoryBoundsEntity;
import com.nflj.gulimall.coupon.service.CategoryBoundsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GulimallCouponApplicationTests {

    @Autowired
    CategoryBoundsService categoryBoundsService;

    @Test
    void contextLoads() {

        CategoryBoundsEntity categoryBoundsEntity = new CategoryBoundsEntity();
        categoryBoundsEntity.setWork(10);
        categoryBoundsService.save(categoryBoundsEntity);
    }

}
