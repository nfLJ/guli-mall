package com.nflj.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nflj.common.utils.PageUtils;
import com.nflj.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:14:35
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

