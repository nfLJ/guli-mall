package com.nflj.gulimall.coupon.dao;

import com.nflj.gulimall.coupon.entity.SkuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品sku积分设置
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:14:35
 */
@Mapper
public interface SkuBoundsDao extends BaseMapper<SkuBoundsEntity> {
	
}
