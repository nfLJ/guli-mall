package com.nflj.gulimall.coupon.dao;

import com.nflj.gulimall.coupon.entity.HomeAdvEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 首页轮播广告
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:14:35
 */
@Mapper
public interface HomeAdvDao extends BaseMapper<HomeAdvEntity> {
	
}
