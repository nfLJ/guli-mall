package com.nflj.gulimall.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 1.如何使用 Nacos 作为配置统一管理配置
 *  1）引入依赖  （nacos-config)
 *  2）创建 bootstrap.yml
 *      配置  spring.applicatiob.name 和 spring.cloud.nacos.config.server-addr
 *  3) 需要给 Nacos 配置中心默认添加一个叫 数据集（Data Id) gulimall-coupon.yml 规则
 *  4）给配置文件添加任何配置
 *  5）动态获取配置
 *     @RefreshScope：动态获取并刷新配置
 *     @Value("${配置项名字}")：获取到配置
 *     如果配置中心和当前应用配置文件中都配置了相同的项，优先使用配置中心的配置
 *
 * 2.细节
 *  1）命名空间：配置隔离
 *      默认：public（保留空间）；默认新增的所有配置都在 public 空间
 *      1、开发、测试、生产：利用命名空间来做环境隔离
 *         注意：在 bootstrap.yml 配置哪个命名空间下的配置
 *         spring.cloud.nacos.config.namespace = 命名空间ID
 *      2、每一个微服务之间互相隔离配置
 *  2）配置集：所有配置的集合
 *  3)配置集ID：类似文件名
 *    Data ID：类似文件名
 *  4）配置分组
 *     默认所有配置集都属于：DEFAULT_GROUP
 *  最佳实践：每个微服务创建自己的命名空间，使用配置分组分环境，dev，test，prod
 *
 * 3.同时加载多个配置集
 *  1）微服务任何配置信息，任何配置文件都可以放在配置中心中
 *  2）只需要 bootstrap.properties 说明加载配置中心的哪些配置文件即可
 *  3）@Value @ConfigurationProperties。。
 *  以前 SpringBoot 任何方法从配置文件中获取值，都能使用
 *  配置中心有优先使用配置中心的
 */

@MapperScan("com.nflj.gulimall.coupon.dao")
@SpringBootApplication
@EnableDiscoveryClient
//@RefreshScope
public class GulimallCouponApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallCouponApplication.class, args);
    }

}
