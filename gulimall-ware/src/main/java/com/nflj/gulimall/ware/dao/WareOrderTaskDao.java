package com.nflj.gulimall.ware.dao;

import com.nflj.gulimall.ware.entity.WareOrderTaskEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 库存工作单
 * 
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:31:16
 */
@Mapper
public interface WareOrderTaskDao extends BaseMapper<WareOrderTaskEntity> {
	
}
