package com.nflj.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author 流觞
 * @version 1.0
 * @date 2021/1/8 18:43
 */
@Data
public class MergeVo {

    private Long purchaseId; //整单id
    private List<Long> items;//[1,2,3,4] //合并项集合
}
