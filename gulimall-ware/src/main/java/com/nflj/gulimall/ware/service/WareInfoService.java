package com.nflj.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nflj.common.utils.PageUtils;
import com.nflj.gulimall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:31:16
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

