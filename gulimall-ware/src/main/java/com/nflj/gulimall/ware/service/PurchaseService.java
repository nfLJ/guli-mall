package com.nflj.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.nflj.common.utils.PageUtils;
import com.nflj.gulimall.ware.entity.PurchaseEntity;
import com.nflj.gulimall.ware.vo.MergeVo;
import com.nflj.gulimall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author nflj
 * @email 838715330@qq.com
 * @date 2021-01-04 22:31:16
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);


    void mergePurchase(MergeVo mergeVo);


    void received(List<Long> ids);


    void done(PurchaseDoneVo doneVo);
}

